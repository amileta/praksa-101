# -*- coding: utf-8 -*-
"""
Created on Sat May 09 14:21:55 2015

Klasa koja opisuje tablicu (koordinate početka, headera, podataka) kako je
pohranjena u PDF datoteci.

@author: Antonio
"""

class TableMetadata:
    
    def __init__(self, layout_height, layout_width, page, table_start, title_top, title_left, title_bottom, title_text):
        
        self.page = page
        self.table_start = table_start
        self.table_end = None
        self.title_top = title_top
        self.title_left = title_left
        self.title_bottom = title_bottom
        self.title = title_text
        self.header_top = None
        self.header_left = None
        self.header_bottom = None
        self.data_top = None
        self.data_left = None
        self.data_bottom = 60
        self.layout_height = layout_height
        self.layout_width = layout_width

    def __str__(self):
        return "\npage_layout_h: " + str(self.layout_height) + "\npage_layout_w:" + str(self.layout_width) + "\npage:" + str(self.page) + "\nstart:" + str(self.table_start)+ "\nend:" + str(self.table_end) + "\ntitle:" + str(self.title.encode('utf-8')) + "\ntitle_left:" + str(self.title_left) + "\ntitle_bottom:" + str(self.title_bottom) + "\ntitle_top:" + str(self.title_top) + "\nheader_left:" + str(self.header_left) + "\nheader_bottom:" + str(self.header_bottom) + "\nheader_top:" + str(self.header_top) + "\ndata_left:" + str(self.data_left) + "\ndata_bottom:" + str(self.data_bottom) + "\ndata_top:" + str(self.data_top)
        
    def __repr__(self):
        return self.__str__()