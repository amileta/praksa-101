# -*- coding: utf-8 -*-
"""
Created on Sun May 10 20:53:57 2015

@author: Antonio
"""

import os

def getOutputPathFromInputPath(currfile, destdir):
    """
    Konvertira ulazni path u izlazni (sa definiranim DESTDIR)
    """
    head,tail = os.path.split(currfile)
    filename = tail.rsplit('.',1)
    filepath = os.path.join(destdir,filename[0])
    
    return filepath