# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 15:56:46 2015

@author: Antonio
"""
import locale
import mechanize
import sys
import os
import math
import numpy as np
from bs4 import BeautifulSoup
#from openpyxl import Workbook
from openpyxl import load_workbook
from pandas import read_html
from pandas import ExcelWriter

#koliko "daleko" (siblings) od elementa tablice cemo trazit naslov
MAX_TITLE_SEARCH_COUNT=6

def is_number(s):
    try:
        if isinstance(s, basestring):
            s = float(s.replace(',','.').replace(' ', ''))
        else:
            s = float(s)
        if (math.isnan(s)):
            return False
        return True
    except ValueError:
        return False

        
#brise sve 'p' tagove koji sadrze samo &nbsp;
def cleanHTML(data):
    soup = BeautifulSoup(data)
    for text in soup.findAll(text="&nbsp;"):
        if text.parent.name == 'p' and len(text.parent.contents) == 1:
            text.parent.extract()
    return unicode(soup)
        
#Provjerava je li tablica s podacima
def isNumericTable(df):
    votes = 0
    #provjerit cemo srednji redak-srednji stupac
    #               zadnji redak-srednji stupac
    #               redak na visini 1/3 -  2. stupac
    #               redak na visini 2/3 - predzadnji stupac
    #               redak na visini 2/3+1 -stupac na duljini 2/3
    #               srednji redak - 1, srednji stupac
    #               srednji redak + 1, srednji stupac
    #               srednji redak, srednji stupac - 1
    #               srednji redak, srednji stupac + 1
    print df.shape
    numrows = (df.shape)[0]
    numcols = (df.shape)[1]
    if numrows == 1 or numcols == 1:
        return False
    #ako su samo dva stupca, provjeri desni
    if numcols == 2:
        sample_cols = [1, 1, 1, 1, 1, 1, 1, 1, 1]
    else:
        sample_cols = [numcols/2, numcols/2, 1, max(0,numcols-2), (2 * numcols) / 3, numcols/2, numcols/2, max(numcols/2 - 1, 1), max(numcols/2 + 1, 1)]
    sample_rows = [numrows / 2, max(0, numrows-2), numrows / 3, (2 * numrows) / 3, min((2 * numrows) / 3 + 1, numrows-1), max (numrows / 2 - 1, 1), min(numrows/2 + 1, numrows - 1), numrows / 2, numrows / 2]
    
    for i in range(9):
        print 'indexi: ' + str(sample_rows[i]) + ',' + str(sample_cols[i])
        if (is_number(df.iloc[sample_rows[i], sample_cols[i]])):
            votes = votes + 1
    if ( votes / 9.0 > 0.5 ):
        print 'Is a data table with votes:' + str(votes)
        return True
    else:
        print 'Not a data table with votes:' + str(votes)
        return False
        
#Otkriva je li tablica s podacima
#TO_DO poboljsat implementaciju (provjera vise celija)
def isDataTable(table):
    if (table.name != 'table'):
        return False;
    rows = table.find_all('tr');
    if rows == 1:
        return False
    #ako ima samo dva retka, provjeri donji
    cols = row.find_all('td');
    numcols = len(cols)
    if (numcols > 2):
        data = (cols[numcols / 2].text).replace(',','.'); #fix za drukciji decimalni separator
    elif numcols == 2 :
        data = (cols[1].text).replace(',','.'); #fix za drukciji decimalni separator
    else:
        return False
    #print data
    if is_number(data):
        #print True;
        return True
    else:
        #print False;
        return False

#izvlaci sadrzaj pocetne tablice (ako je cijeli dokument u tablici)
def extractFromOuterTable(soup):
    #nadji prvu tablicu
    table = soup.find('table')
    if ((table.parent.name == 'div' and (table.parent['class'][0] != 'Section1' if 'class' in table.parent.attrs else True) ) or table.parent == 'body' ):
        return table.extract()
    else:
        return soup

#parse tablica (za svaku tablicu stvara novi worksheet u danom workbooku)
#TO_DO skuzit kako sacuvat naslove tablica (gdje ih spremit - ime datoteke ocito nije najbolja ideja)
def parseTables(curr_workbook, csvpath, soup, ):
    #nadji vanjsku tablicu(ako postoji)
    soup = extractFromOuterTable(soup);
    #nadji sve tablice
    tables = soup.find_all('table')
    k = 0
    for table in tables:
            k=k+1
            table = extractTableWithTitle(table);
            if (table != None):
                DataTable = read_html(str(table), thousands = None, encoding='utf-8');
            else:
                continue
            df = DataTable[0].dropna(how='all')
            if not df.empty and isNumericTable(df):
                print 'saving table' + str(DataTable[0])
                #pattern = re.compile(r"[\n\t\v\f\r\xA0\u00a0\u200b\\]+")
                #sheetname = pattern.sub("",table.p.text)          
                df.to_excel(curr_workbook, sheet_name = str(k), header = False, index = False);
                df.to_csv(csvpath+'-'+str(k)+'.csv', header = False, index = False, encoding='utf-8');
    curr_workbook.save()

#izvlaci tablicu sa naslovom
def extractTableWithTitle(table):
    title_el = table.findPrevious('p')
    title = ''
    elements = 0
    #izvlaci naslov, u jednom redu
    while True:
        elements = elements + 1
        if elements > MAX_TITLE_SEARCH_COUNT:
            break;
        if (title_el == None or title_el.text == u'\xa0'):
            continue;
        title = title_el.text + title
        title_el = title_el.find_previous_sibling('p')
    return table;

#pretvori tablicu u excel,append ili create (cuva naslov, u jednom redu) (DEPRECATED)
def htmlToXLS(table, worksheet, mode):
    rows = table.findAll('tr')
    title_el = table.findPrevious('p')
    title = ''
    elements = 0
    while True:
        elements = elements + 1
        if elements > MAX_TITLE_SEARCH_COUNT:
            break;
        if (title_el == None or title_el.text == u'\xa0'):
            continue;
        title = title_el.text + title
        title_el = title_el.find_previous_sibling('p')
    if mode == 'create':
        x = 1
        y = 1
        worksheet.cell(row = x, column = y).value = title
        x = x+1
        for tr in rows:
            cols = tr.findAll('td')
            y = 1
            for td in cols:
                text_xls = td.text;
                text_xls = text_xls.encode('utf-8')
                text_xls = text_xls.strip()
                worksheet.cell(row = x, column = y).value = text_xls
                y = y + 1
            x = x + 1
    elif mode == 'append':
        for tr in rows:
            cols = table.findAll('td')
            y = 0
        for td in cols:
            text_xls = td.text;
            text_xls = text_xls.encode('utf-8')
            text_xls = text_xls.strip()
            worksheet.write(x,y, text_xls)
            y = y + 1
        x = x + 1
        

#citanje fajla s linkovima
#prvi argument je fajl, drugi path di se sprema
locale.setlocale(locale.LC_ALL,"Croatian") 

if sys.argv[1]:
    if os.path.isfile(sys.argv[1]):
        try:
            workbook = load_workbook(sys.argv[1]);
        except:
            "Excel datoteka ne postoji!"
    else:
        print "Datoteka ne postoji!"
        exit
    if sys.argv[2]:
        try: 
            os.makedirs(sys.argv[2])
            PATH = str(sys.argv[2])
        except OSError:
            if not os.path.isdir(sys.argv[2]):
                raise
    else:
        print ("Niste unijeli putanju do direktorija podataka!")
        raise OSError
            
           

#inicijalizacija browsera
br = mechanize.Browser(factory=mechanize.RobustFactory())
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

#otvaranje worksheeta
worksheets = workbook.sheetnames
#za svaki worksheet isparsaj i stvori novi folder
for sheet in worksheets:
    if sheet == 'Sheet':
        continue
    sheet = workbook.get_sheet_by_name(sheet)
    try: 
        #za svaki sheet napravi folder (za publikaciju)
        os.makedirs(sys.argv[2] + '/' + sheet.title)
    except OSError:
        if not os.path.isdir(sheet.title):
            raise
    #petlja za iteraciju po html fajlovima (redovima), za svaki red novi workbook
    for row in sheet.rows:
        #curr_workbook = Workbook()
        filepath = sys.argv[2] + '/'+str(sheet.title)+'/'+(row[0].value)[-5:-1] + '_' + (row[1].value)[:-1].replace('.','_').replace(' ', '_').replace('/', '_')
        curr_workbook = ExcelWriter(filepath + '.xlsx');
        br.open(str(row[3].value))
        #pretvorba responsa u BS objekt
        soup = BeautifulSoup(br.response().read(), from_encoding='utf-8')
        #parse tablica (za svaku podatkovnu tablicu stvara novi worksheet)
        parseTables(curr_workbook, filepath, soup)
        print sheet.title + '/' + row[0].value
workbook.close()