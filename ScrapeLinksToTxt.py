# -*- coding: utf-8 -*-
"""
Created on Wed Apr 08 14:24:14 2015

Skripta za dohvacanje svih linkova HTML publikacija sa DZS
Dohvaca linkove u tekst datoteke, grupirane prema sifri publikacije

@author: Antonio
"""

import mechanize
import os
from bs4 import BeautifulSoup
import re

#inicijalizacija browsera
br = mechanize.Browser(factory=mechanize.RobustFactory())
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
br.open("http://www.dzs.hr/app/kalendar/PubBySubject.aspx")

#odabir forme
br.select_form(nr=0)
br.set_all_readonly(False)

#odabir checkboxa za sve godine
br["__EVENTTARGET"] = "CheckBox1"
br["__EVENTARGUMENT"] = ""
br["CheckBox1"] = ["on"]
br.submit()

#pretvorba responsa u BS objekt
soup = BeautifulSoup(br.response().read())

#pronalazak popisa godina
years = soup.find(id="DropDownList5").find_all('option')

def slugify(value):
    """
    Normalizes string, removes non-alpha characters,
    and converts whitespaces to hyphens.
    """
    import unicodedata
    value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore')
    value = unicode(re.sub('[^\w\s-]', '', value).strip())
    return re.sub('[-\s]+', '-', value)

#za trazenje linkova na dokumente (da izbacimo one koji nisu objavljeni)
def isValidLink(tag):
    href = tag and tag.get('href')
    if (not href):
        return 0
    return all([
        tag,
        tag.name == 'a',
        tag.parent.name == 'span',
        href,
        not tag.has_attr('no_track'),
        not href.startswith('mailto'),
        not href.startswith('#')
    ])
    
#duplikati (publikacije koje nemaju konačne podatke)
def isDupe(string):
    if string[-4:] == 'corr':
            return True
    for dupe in dupes:
        if dupe == string:
            return True
        #ako je publikacija s korekcijom, a imamo je 
        #elif dupe[-4:] == 'corr' and dupe[:-4] == string[:-4]:
        #    return True
        #ako je pub s revizijom, a imamo je 
        elif dupe[-1].isalpha and string[-1].isalpha and dupe[:-1] == string[:-1]:
            return True
        #ako je pub s revizijom, a imamo je 
        elif dupe[-1].isalpha and dupe[:-1] == string:
            return True
    return False

#detekcija starijeg duplikata (tj publikacije ciji link smo vec pohranili, a postoji stariji)
def isOlderDupe(string):
    #ako nema na kraju slovo, onda su stariji podaci
    if not string[-1].isalpha:
        return True
    for dupe in dupes:
        #ako su iste, nebitno
        if dupe == string:
            return True
        #ako je pub bez slova onda je stariji 
        elif dupe[-1].isalpha and not string[-1].isalpha:
            return True
        #ako je isti pub, s revizijom, a manje slovo po abecedi, onda je stariji 
        elif dupe[-1].isalpha and string[-1].isalpha and dupe > string:
            return True
        #inace nije stariji od ovog kog vec imamo zabiljezenog
        else:
            return False

dupes = []
path = './LINKS/HTML_prema_programu/'
pdfpath = './LINKS/PDF_prema_programu/'
#petlja za godine
for y in years:
    #stvaranje foldera za godinu
    #try: 
    #    os.makedirs(path + y['value'])
    #except OSError:
    #    if not os.path.isdir(path + y['value']):
    #        raise
    print y['value']
    #if int(y['value']) == 2014:
    br.select_form(nr=0)
    br.set_all_readonly(False)
    br["DropDownList5"] = [y['value']]
    br.submit()
    soup = BeautifulSoup(br.response().read())
    stat_areas = soup.find(id="DropDownList6").find_all('option')
    #petlja za podrucja
    for sa in stat_areas:
        #if int(sa['value']) == 1:
            #stvaranje foldera za podrucje
            #try: 
            #    os.makedirs(path  + '/' + sa['value'])
            #except OSError:
            #    if not os.path.isdir(path  + '/' + sa['value']):
            #        raise
            row = 0;
            br.select_form(nr=0)
            br.set_all_readonly(False)
            br["DropDownList5"] = [y['value']]
            br["DropDownList6"] = [sa['value']]
            br.submit()
            soup = BeautifulSoup(br.response().read())
            stat_subareas = soup.find(id="DropDownList7").find_all('option')
            #petlja za podpodrucja
            for ss in stat_subareas:
                #stvaranje foldera za podpodrucja
                #try: 
                #   os.makedirs(path +'/' + sa['value'] + '/' + ss['value'])
                #except OSError:
                #    if not os.path.isdir(path + '/' + sa['value'] + '/' + ss['value']):
                #        raise
                br.select_form(nr=0)
                br.set_all_readonly(False)
                br["DropDownList5"] = [y['value']]
                br["DropDownList6"] = [sa['value']]
                br["DropDownList7"] = [ss['value']]
                br.submit()
                soup = BeautifulSoup(br.response().read())
                publications = soup.find(id="DropDownList8").find_all('option')
                #petlja za publikacije u podpodrucjima
                for pub in publications:
                    #stvaranje foldera za publikacije (ignoriraj one koje imaju vecu vrijednost od 1000, to su pdfovi)
                    #if int(pub['value']) < 1000:
                        #try: 
                        #   os.makedirs(path + '/'+ sa['value'] + '/' + ss['value'] + '/' + pub['value'])
                        #except OSError:
                        #    if not os.path.isdir(path + '/' + sa['value'] + '/' + ss['value'] + '/' + pub['value']):
                        #        raise
                        #dirpath = path + '/' + sa['value'] + '/' + ss['value'] + '/' + pub['value'] + '/'
                    #else:
                        #continue
                    br.select_form(nr=0)
                    br.set_all_readonly(False)
                    br["DropDownList5"] = [y['value']]
                    br["DropDownList6"] = [sa['value']]
                    br["DropDownList7"] = [ss['value']]
                    br["DropDownList8"] = [pub['value']]
                    br.submit();
                    #print pub //debug
                    soup = BeautifulSoup(br.response().read())
                    #pronadji tablicu s publikacijama
                    table = soup.find(id="DataList1")        
                    #nadji sve aktivne linkove
                    for link in soup.find_all(isValidLink):
                        string = link['href']
                        #identifikator dokumenta
                        di01 = string.rsplit('/',1)
                        di02 = di01[1].split('.',1)
                        #print document
                        #ignore pdfs
                        if (di02[1] != 'pdf'):
                            #makni godinu iz linka
                            document = di02[0].rsplit('_', 1)
                            #check for dupes, if its not a dupe or is a correction
                            if (not isDupe(document[0]) or (document[1])[-4:] == 'corr'):
                                #this is ridiculous, find a better way
                                designation= link.parent.parent.findPrevious('td')
                                date = designation.findPrevious('td').findPrevious('td').span.getText()
                                text= link.getText()
                                string = string.replace("../..", "http://www.dzs.hr")
                                #save to text file
                                pub_class = di02[0].rsplit('_', 2);
                                storepath = os.path.join(y['value'], sa['value'], ss['value'], pub['value'])
                                with open(path + str(int(sa['value'])) +'-'+  str(int(ss['value'])) + '-' + str(int(pub['value'])) + '.txt', "a") as code:                                   
                                   code.write(storepath + '|'+ date.encode("utf-8") +'|' + designation.getText().encode("utf-8") + '|' + text.encode("utf-8") + '|' + string  + '\n')
                            #if it's a dupe and we don't already have a newer dupe remember it
                            if (((document[0])[-1].isalpha() and not isOlderDupe(document[0]))):
                                dupes.append(document[0])
                    #clear dupes for this pub
                    dupes.remove