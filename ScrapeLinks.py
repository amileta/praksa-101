# -*- coding: utf-8 -*-
"""
Created on Wed Mar 25 19:53:48 2015

Preuzima sve linkove sa DZS - prema programu publikacije i pohranjuje u Excel datoteke

@author: Antonio
"""

import mechanize
import os
from bs4 import BeautifulSoup
from openpyxl import load_workbook
from openpyxl import Workbook

#inicijalizacija browsera
br = mechanize.Browser(factory=mechanize.RobustFactory())
br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
br.open("http://www.dzs.hr/app/kalendar/PubBySubject.aspx")

#odabir forme
br.select_form(nr=0)
br.set_all_readonly(False)

#odabir checkboxa za sve godine
br["__EVENTTARGET"] = "CheckBox1"
br["__EVENTARGUMENT"] = ""
br["CheckBox1"] = ["on"]
br.submit()

#pretvorba responsa u BS objekt
soup = BeautifulSoup(br.response().read())

#pronalazak popisa godina
years = soup.find(id="DropDownList5").find_all('option')

#za trazenje linkova na dokumente (da izbacimo one koji nisu objavljeni)
def isValidLink(tag):
    href = tag and tag.get('href')
    if (not href):
        return 0
    return all([
        tag,
        tag.name == 'a',
        tag.parent.name == 'span',
        href,
        not tag.has_attr('no_track'),
        not href.startswith('mailto'),
        not href.startswith('#')
    ])
    
#duplikati (publikacije koje nemaju konačne podatke)
def isDupe(string):
    for dupe in dupes:
        if dupe == string:
            return True
        #ako je publikacija s korekcijom, a imamo je 
        elif dupe[-4:] == 'corr' and dupe[:-4] == string[:-4]:
            return True
        #ako je pub s revizijom, a imamo je 
        elif dupe[-1].isalpha and string[-1].isalpha and dupe[:-1] == string[:-1]:
            return True
        #ako je pub s revizijom, a imamo je 
        elif dupe[-1].isalpha and dupe[:-1] == string:
            return True
    return False

#detekcija starijeg duplikata
def isOlderDupe(string):
    #ako nema na kraju slovo, onda su stariji podaci
    if not string[-1].isalpha:
        return True
    for dupe in dupes:
        #ako su iste, nebitno
        if dupe == string:
            return True
        #ako je isti pub, bez slova onda je stariji 
        elif dupe[-1].isalpha and not string[-1].isalpha:
            return True
        #ako je isti pub, s revizijom, a manje slovo, onda je stariji 
        elif dupe[-1].isalpha and string[-1].isalpha and dupe > string:
            return True
        #inace nije stariji od ovog kog imamo
        else:
            return False

dupes = []
path = './DATA/'
#petlja za godine
for y in years:
    print y['value']
    #if int(y['value']) == 2014 or int(y['value']) == 2013: //DEBUG
    br.select_form(nr=0)
    br.set_all_readonly(False)
    br["DropDownList5"] = [y['value']]
    br.submit()
    soup = BeautifulSoup(br.response().read())
    stat_areas = soup.find(id="DropDownList6").find_all('option')
    #petlja za podrucja
    for sa in stat_areas:
        #if int(sa['value']) == 1:
            #stvaranje foldera za podrucje
            try: 
                os.makedirs(path + sa['value'])
            except OSError:
                if not os.path.isdir(path + sa['value']):
                    raise
            dirpath = path + sa['value'] + '/'
            row = 0;
            br.select_form(nr=0)
            br.set_all_readonly(False)
            br["DropDownList5"] = [y['value']]
            br["DropDownList6"] = [sa['value']]
            br.submit()
            soup = BeautifulSoup(br.response().read())
            stat_subareas = soup.find(id="DropDownList7").find_all('option')
            #petlja za podpodrucja
            for ss in stat_subareas:
                #otvaranje workbooka
                try:
                    workbook = load_workbook(dirpath + ss['value'] + '.xlsx');
                except:
                    workbook = Workbook()
                br.select_form(nr=0)
                br.set_all_readonly(False)
                br["DropDownList5"] = [y['value']]
                br["DropDownList6"] = [sa['value']]
                br["DropDownList7"] = [ss['value']]
                br.submit()
                soup = BeautifulSoup(br.response().read())
                publications = soup.find(id="DropDownList8").find_all('option')
                #petlja za publikacije u podpodrucjima
                for pub in publications:
                    #provjera otvorenosti workbooka
                    if workbook:
                        #provjera worksheeta za publikaciju
                        worksheet = workbook.get_sheet_by_name(pub['value']);
                        #inace napravi novi sheet                        
                        if (worksheet == None):
                            if (int(pub['value']) > 1000):
                                continue;
                            worksheet = workbook.create_sheet(title=pub['value'])
                    else:
                        raise Exception('Workbook not created!')
                    br.select_form(nr=0)
                    br.set_all_readonly(False)
                    br["DropDownList5"] = [y['value']]
                    br["DropDownList6"] = [sa['value']]
                    br["DropDownList7"] = [ss['value']]
                    br["DropDownList8"] = [pub['value']]
                    br.submit();
                    #print pub //debug
                    soup = BeautifulSoup(br.response().read())
                    table = soup.find(id="DataList1")
                    links = []                    
                    for link in soup.find_all(isValidLink):
                        string = link['href']
                        #identifikator dokumenta
                        di01 = string.rsplit('/',1)
                        di02 = di01[1].split('.',1)
                        #print document
                        #ignore pdfs
                        if (di02[1] != 'pdf'):
                            #makni godinu iz linka
                            document = di02[0].rsplit('_', 1)
                            #check for dupes
                            if (not isDupe(document[0]) or (document[1])[-4:] == 'corr'):
                                links.append(string.replace("../..", "http://www.dzs.hr"))
                                #this is ridiculous, find a better way
                                designation= link.parent.parent.findPrevious('td')
                                date = designation.findPrevious('td').findPrevious('td').span.getText()
                                text= link.getText()
                                string = string.replace("../..", "http://www.dzs.hr")
                                #append the date, name and link to the publication worksheet                                    
                                worksheet.append([str(date),str(designation.span.getText()),text,string])
                            #if it's a dupe and we don't already have a newer dupe remember it
                            if (((document[0])[-1].isalpha() and not isOlderDupe(document[0])) or (document[1])[-4:] == 'corr'):
                                dupes.append(document[0])
                    #print links //debug
                    #print '#---------------#'
                    #clear dupes for this pub //debug
                    dupes.remove
                workbook.save(dirpath + ss['value'] + '.xlsx')
