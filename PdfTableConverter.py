# -*- coding: utf-8 -*-
"""
Created on Sat May 09 15:37:10 2015

@author: Antonio
"""
import subprocess
import TableTitleCoordinateFinder
import logging
import os
import sys
import getopt

from pandas import read_csv
from pandas import ExcelWriter

from HelperFunctions import getOutputPathFromInputPath

#print TableTitleCoordinateFinder.tables

def convert(k, table, logger):
    
    """
    Konvertira tablicu definiranu metapodacima u table u excel dataframe.
    Poziva tabula-java knjiznicu za konverziju sa koordinatama koje su pronadjene u prethodnom koraku.
    Obraduje zasebno podrucje naslova, headera i podataka, kako su pronadjena u prethodnom koraku.
    Podaci se zapisuju u privremene datoteke. (TODO obrisat ih nakon sta program zavrsi)
    Na kraju spaja konvertirane tablice u jedan dataframe.
    """
    
    title_area = str(table.layout_height - table.title_top)+',0,'+str(table.layout_height - table.title_bottom)+','+str(table.layout_width)
    success = subprocess.call(['java', '-jar', 'E:\\Dropbox\\CLER-materijali za praksu\\TOOLS\\tabula-java-master\\target\\tabula-extractor-0.7.4-SNAPSHOT-jar-with-dependencies.jar', 'E:\\praksa-101\\SI-1485-14.pdf', '-a', str(title_area) ,'-o', 'E:\\praksa-101\\title_out.txt', '-p', str(table.page), '-n', '-g'])
    print success    
    if success == 0:
        logger.info("Uspješno konvertiran naslov tablice %d:%s", k, table.title.encode('utf-8'))   
    else:
        logger.error("Konverzija naslovnog dijela tablice %d:%s nije uspjela", k, table.title.encode('utf-8'))   
    header_area = str(table.layout_height - table.header_top)+',0,'+str(table.layout_height - table.header_bottom)+','+str(table.layout_width)
    success = subprocess.call(['java', '-jar', 'E:\\Dropbox\\CLER-materijali za praksu\\TOOLS\\tabula-java-master\\target\\tabula-extractor-0.7.4-SNAPSHOT-jar-with-dependencies.jar', 'E:\\praksa-101\\SI-1485-14.pdf', '-a', str(header_area) ,'-o', 'E:\\praksa-101\\header_out.txt', '-p', str(table.page), '-n'])
    print success    
    if success == 0:
        logger.info("Uspješno konvertiran header tablice %d:%s", k, table.title.encode('utf-8'))   
    else:
        logger.error("Konverzija headera tablice %d:%s nije uspjela", k, table.title.encode('utf-8'))  
    data_area = str(table.layout_height - table.data_top)+',0,'+str(table.layout_height - table.data_bottom)+','+str(table.layout_width)
    success = subprocess.call(['java', '-jar', 'E:\\Dropbox\\CLER-materijali za praksu\\TOOLS\\tabula-java-master\\target\\tabula-extractor-0.7.4-SNAPSHOT-jar-with-dependencies.jar', 'E:\\praksa-101\\SI-1485-14.pdf', '-a', str(data_area) ,'-o', 'E:\\praksa-101\\data_out.txt', '-p', str(table.page), '-n', '-g'])
    print success    
    if success == 0:
        logger.info("Uspješno konvertirani podaci tablice %d:%s", k, table.title.encode('utf-8'))
    else:
        logger.error("Konverzija podataka tablice %d:%s nije uspjela", table.title.encode('utf-8'))  
    try:
        DF1 = read_csv("E:\\praksa-101\\title_out.txt", header = None, encoding = 'cp1250')
        DF1.dropna(how="all",axis=[1], inplace=True)
    except ValueError:
        logger.warning("Naslov tablice %s je prazan", table.title)
    try:
        DF2 = read_csv("E:\\praksa-101\\header_out.txt", header = None, encoding = 'cp1250')
        DF2.dropna(how="all",axis=[1], inplace=True)
    except ValueError:
        logger.warning("Header tablice %s je prazan", table.title)
    try:
        DF3 = read_csv("E:\\praksa-101\\data_out.txt", header = None, encoding = 'cp1250')
        DF3.dropna(how="all",axis=[1], inplace=True)
    except ValueError:
        logger.warning("Podaci tablice %s su prazni", table.title)
    
    if DF1 is not None and DF2 is not None and DF3 is not None:
        DF3 = (DF1.append(DF2,ignore_index=True)).append(DF3,ignore_index=True)
    elif DF1 is not None and DF2 is not None:
        DF3 = (DF1.append(DF2,ignore_index=True))
    elif DF1 is not None and DF3 is not None:
        DF3 = (DF1.append(DF3,ignore_index=True))
    elif DF2 is not None and DF3 is not None:
        DF3 = (DF2.append(DF3,ignore_index=True))
    return DF3
    
def main(argv):
    # The top argument for walk
    TOPDIR = '.'
    # The extension to search for
    EXTEN = '.pdf'
    # Directories to ignore
    IGNORE = []
    #logfile for processed files
    CONVERTEDLOG = 'converted.log'   
    #root directory for processed files (TODO napravit konfigurable output dir)
    DONEDIR = './DATA/PDF_konvertiran_prema_programu'
    
    ### COMMAND LINE OPCIJE ###
    
    try:
        opts, args = getopt.getopt(argv,"hd:e:l:i:")
    except getopt.GetoptError:
        print 'PdfTableConverter.py -d <startdir> -l <logfile> -i <ignorestring>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'PdfTableConverter.py -d <startdir> -e <extension>'
            sys.exit()
        elif opt in ("-d"):
            TOPDIR = arg
        elif opt in ("-l"):
            CONVERTEDLOG = arg
        elif opt in ("-i"):
            IGNORE = arg.split(',')
    
    ### LOGGING CONFIGURATION ###
    
    # create logger
    logger = logging.getLogger('pdf_conversion_logger')
    logger.setLevel(logging.DEBUG)
    # create console handler and set level to debug
    ch = logging.FileHandler(CONVERTEDLOG)
    ch.setLevel(logging.DEBUG)
    
    # create formatter
    formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')
    
    # add formatter to ch
    ch.setFormatter(formatter)
    
    # add ch to logger
    logger.addHandler(ch)

    
    ### RECURSIVE PROCESS ###
    for dirpath, dirnames, files in os.walk(TOPDIR):
        # Remove directories in ignore
        # directory names must match exactly!
        for idir in IGNORE:
            if idir in dirnames:
                dirnames.remove(idir)
        #loop through files
        for name in files:
            if name.lower().endswith(EXTEN):
                logger.info(u"Processsing:" + unicode(name) + u'\n')
                #call the find function for that file
                tables = TableTitleCoordinateFinder.find(name,dirpath,logger)
                filepath = getOutputPathFromInputPath(name, dirpath)
                curr_workbook = ExcelWriter(filepath + '.xlsx');
                k = 0
                for table in tables:
                    #convert the table and save to workbook
                    dataframe = convert(k, table, logger)
                    #write it to a xls file
                    dataframe.to_excel(curr_workbook, sheet_name = unicode(k), header = False, index = False, encoding='utf-8');
                    curr_workbook.save()
                    #dataframe.to_csv(name+str(k)+".csv", header = False, index = False);
                    k+=1
                curr_workbook.close()
                    
### PROGRAN START ###

if __name__ == "__main__":
   main(sys.argv[1:])               
                