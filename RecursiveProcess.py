# -*- coding: utf-8 -*-
"""
Created on Thu Apr 09 20:04:42 2015

Skripta za rekurzivno pozivanje procesuiranja HTML datoteka kroz datotečnu
hijerarhiju

@author: Antonio
"""

import os
import ScrapeTablesFileSystem
import logging
import sys
 
# The top argument for walk
topdir = '.'
# The extension to search for
EXTEN = '.htm'
# What will be logged
results = []
#a list for found files
found = []
#root directory for processed files
DONEDIR = './DATA/Podaci_prema_programu'
# Directories to ignore
IGNORE = ['Podaci_prema_programu','IGNORE', '2000','2001','2002', '2003', '2005','2006','2007','2008', '2009','2010','2011','2012', '2013', '2014', '2015']
#logfile for processed files
PROCESSEDLOG = './tmp/processed.log'
#logfile for processed filepaths
PROCESSEDPATHS = './tmp/processed_filepaths.log'
#logfile for debugging
ERRORLOG = './tmp/exception.log'

class StreamToLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
 
   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())


def extractHierarchy(string):
    """
    function to get the folder path from current directory
    """
    groups = string.rsplit('\\', 4)
    return os.path.join(DONEDIR, '\\'.join(groups[-4:]))

def process(filename, dirpath, logger):
    """
    process found file
    """
    try:
        ScrapeTablesFileSystem.convertHTMLfile(os.path.join(dirpath, name), extractHierarchy(dirpath), logger)
        #logging.basicConfig(level=logging.DEBUG, filename=ERRORLOG)
    except Exception as ex:
        print ex.message
        logging.exception("Error processing file: " + filename + ":\n" +ex.message + '\n')

### PROVJERE POSTAVKI ###
if DONEDIR:
    try: 
        os.mkdir(DONEDIR);
    except OSError:
        if not os.path.isdir(DONEDIR):
            print ("Niste unijeli putanju do DIREKTORIJA za rezultate obrade!")
            logging.exception("Not a filepath!: %s :\n" , os.path.abspath(DONEDIR))
            raise OSError ("Nedefinirana putanja do direktorija za obradu podataka!")
else:
    raise OSError ("Nedefinirana putanja do log datoteke")

"""
if ERRORLOG:
    try: 
        #errorlog = open(ERRORLOG, 'a');
    except OSError:
        if not os.path.isfile(ERRORLOG):
            print ("Niste unijeli putanju do DATOTEKE za dnevnik gresaka!")
            logging.exception("Not a filepath!: %s :\n" , os.path.abspath(ERRORLOG))
            raise OSError
else:
    raise OSError ("Nedefinirana putanja do log datoteke")
"""
 
if PROCESSEDLOG:
    try: 
        processedlog = open(PROCESSEDLOG, 'a');
    except OSError:
        if not os.path.isfile(PROCESSEDLOG):
            print ("Niste unijeli putanju do DATOTEKE za dnevnik obrade!")
            logging.exception("Not a filepath!: %s :\n" , os.path.abspath(PROCESSEDLOG))
            raise OSError ("Nedefinirana putanja do log datoteke za dnevnik obrade")
else:
    raise OSError ("Nedefinirana putanja do log datoteke za dnevnik obrade")
if PROCESSEDPATHS:
    try: 
        processedpaths = open(PROCESSEDPATHS, 'a');
    except OSError:
        if not os.path.isfile(PROCESSEDPATHS):
            print ("Niste unijeli putanju do DATOTEKE za dnevnik obradenih putanja!")
            logging.exception("Not a filepath!: %s :\n" , os.path.abspath(PROCESSEDPATHS))
            raise OSError ("Nedefinirana putanja do log datoteke za dnevnik obradenih putanja")
else:
    raise OSError ("Nedefinirana putanja do log datoteke za obradene putanje")

### LOGGING CONFIGURATION ###
# create logger
logger = logging.getLogger('scraping_logger')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.FileHandler(PROCESSEDLOG)
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

#redirect everything to logger (to pick up warnings by pandas read_table)
sl = StreamToLogger(logger, logging.INFO)
sys.stdout = sl
 
sl = StreamToLogger(logger, logging.ERROR)
sys.stderr = sl

 
### RECURSIVE PROCESS ###
for dirpath, dirnames, files in os.walk(topdir):
    # Remove directories in ignore
    # directory names must match exactly!
    for idir in IGNORE:
        if idir in dirnames:
            dirnames.remove(idir)
    #loop through files
    for name in files:
        if name.lower().endswith(EXTEN):
            #call the process function for that file
            process(name,dirpath, logger)
            # Save to results string instead of printing
            results += '%s\n' % os.path.join(os.path.abspath(dirpath), name)
    processedpaths.write(str(results))
    results=''

### RELEASE RESOURCES, RESET stderr, stdout ###
processedlog.close()
processedpaths.close()
logger.removeHandler(ch)
ch.close()
sys.stdout = sys.__stdout__
sys.stderr = sys.__stderr__