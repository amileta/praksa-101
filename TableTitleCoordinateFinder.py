# -*- coding: utf-8 -*-
"""
Created on Sat May 09 11:53:28 2015

Skripta koja pronalazi koordinate svih objekata koji su (vjerojatno) naslovi tablica.

@author: Antonio
"""

from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator
import pdfminer.layout

import re
import os

from TableDescription import TableMetadata

#compile regex for finding page titles
regex = re.compile("^\s*\d+\.\d*.?\d*\.?\s*[A-ZŽČĆŠĐ]+")
#compile regex for finding footer notes
footerregex = re.compile("\d+\)\s+[A-ZŠĐČĆŽ][A-zŠĐČĆŽšđđčćž]+")
# An array of TableDescription objects
tables = []
    
def parse_obj(lt_objs, pageno, layout):

    #brojac debljih crta, da znamo za koju tablicu spremamo koordinate u slucaju vise tablica na jednoj stranici
    line_counter = -1    
    
    # loop over the object list
    for obj in lt_objs:
        if isinstance(obj, pdfminer.layout.LTChar):
            text = obj.get_text()
            print '<'+text +'>'
        # if it's a textbox, print text location AND page number
        if isinstance(obj, pdfminer.layout.LTTextBox):
            text = obj.get_text()#.replace('\n', '_'
            print text +'|'
            if regex.match(text):
                if (len(tables) > 0):
                    #ako je na novoj stranici, uzmi prethodnu do kraja (do footera, ako postoji)
                    if tables[-1].page < pageno:
                        if tables[-1].data_bottom is not None:
                            tables[-1].data_bottom = 60
                    else:
                        #inace uzmi do novog naslova
                        tables[-1].data_bottom = obj.bbox[3]
                        tables[-1].table_end = obj.bbox[3]
                tables.append(TableMetadata(layout.height, layout.width, pageno, obj.bbox[3],  obj.bbox[3], obj.bbox[0], obj.bbox[1], text))
                print "Tables contains :" + str(len(tables))
            elif footerregex.match(text):
                if tables[-1].data_bottom is None or tables[-1].data_bottom < obj.bbox[3]:
                    tables[-1].data_bottom = obj.bbox[3] + 5

        #0 je lijeva stranica, a 3 gornja

                print "%d, %6d, %6d, %s" % (pageno, obj.bbox[0], obj.bbox[3], obj.get_text()) #, text)
        #ako je linija
        elif isinstance(obj, pdfminer.layout.LTLine):
                print "%d, %6d, %6d, linija" % (pageno, obj.bbox[0], obj.bbox[3])#, text)
        #ako je rectangle (crta) koja pocinje prije ili u razini naslova i debljine je izmedu 1 i 2
        elif isinstance(obj, pdfminer.layout.LTRect):
            
            if len(tables) >= 1 and obj.bbox[0] <= tables[-1].title_left and obj.height <= 2 and obj.height > 1 :
                print "%d, %6d, %6d, %f" % (pageno, obj.bbox[0], obj.bbox[3], obj.height)#, text)
                line_counter+=1
                print "LC: " + str(line_counter)
                tables[line_counter].title_bottom = obj.bbox[3]                
                tables[line_counter].header_left = obj.bbox[0]
                tables[line_counter].header_top = obj.bbox[3]
                tables[line_counter].header_bottom = None
                #ako je rectangle (crta) koja pocinje prije ili u razini naslova i debljine je izmedu 0 i 1
            
            elif len(tables) >= 1 and obj.bbox[0] <= tables[line_counter].title_left and obj.bbox[1] >= 60 and obj.height <= 1 and obj.height > 0 and (tables[line_counter].data_top is None or obj.bbox[1] < tables[line_counter].data_top):
                print "%d, %6d, %6d, %f" % (pageno, obj.bbox[0], obj.bbox[3], obj.height)#, text)
                tables[line_counter].header_bottom = obj.bbox[1]                
                tables[line_counter].data_left = obj.bbox[0]
                tables[line_counter].data_top = obj.bbox[1]
                #tables[-1].data_bottom = None
            
        # if it's a container, recurse
        elif isinstance(obj, pdfminer.layout.LTFigure):
            print "figure detected"
            parse_obj(obj._objs, pageno, layout)

def find(name, dirpath, logger): 
    
    # Open a PDF file.
    fp = open(os.path.join(os.path.abspath(dirpath), name), 'rb')
    
    # Create a PDF parser object associated with the file object.
    parser = PDFParser(fp)
    
    # Create a PDF document object that stores the document structure.
    # Password for initialization as 2nd parameter
    document = PDFDocument(parser)
    
    # Check if the document allows text extraction. If not, abort.
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed
    
    # Create a PDF resource manager object that stores shared resources.
    rsrcmgr = PDFResourceManager()
    
    # Create a PDF device object.
    device = PDFDevice(rsrcmgr)
    
    # BEGIN LAYOUT ANALYSIS
    # Set parameters for analysis.
    laparams = LAParams(line_overlap=0.1,
                     char_margin=2.0,
                     line_margin=0.4,
                     word_margin=0.2,
                     boxes_flow=0.6,
                     all_texts=True)
    
    # Create a PDF page aggregator object.
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)
    
    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    
    # loop over all pages in the document
    pageno = 0
    layout = None
    for page in PDFPage.create_pages(document):
        pageno+=1
        # read the page into a layout object
        interpreter.process_page(page)
        layout = device.get_result()
        
        # extract text from this object
        parse_obj(layout._objs, pageno, layout)
    print tables
    return tables