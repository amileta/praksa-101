# -*- coding: utf-8 -*-
"""
Created on Mon Mar 30 15:56:46 2015

Skripta za konvertiranje tablica iz HTML datoteka spremljenih na hard disku

@author: Antonio
"""
import locale
import os
import math
import StringIO
import array
from bs4 import BeautifulSoup
from pandas import ExcelWriter
from pandas import read_table
from pandas import DataFrame
from HelperFunctions import getOutputPathFromInputPath


#koliko "daleko" (siblings) od elementa tablice cemo trazit naslov
MAX_TITLE_SEARCH_COUNT=8

#koliko uzoraka koji su brojevi je potrebno za detekciju brojevne tablice
VOTES_THRESHOLD = 3

soup = 0

def is_number(s):
    """
    provjerava je li dani parametar broj
    """
    try:
        if isinstance(s, basestring):
            #crtice se isto broje kao podaci
            if (s.strip() == '-'):
                return True
            s = float(s.replace(',','.').replace(' ', ''))
        else:
            s = float(s)
        if (math.isnan(s)):
            return False
        return True
    except ValueError:
        return False

        
def cleanHTML(data):
    "brise sve 'p' tagove koji sadrze samo &nbsp;"
    soup = BeautifulSoup(data)
    for text in soup.findAll(text="&nbsp;"):
        if text.parent.name == 'p' and len(text.parent.contents) == 1:
            text.parent.extract()
    return unicode(soup)

def isNumericTable(df, title, logger):
    votes = 0
    """
    Provjerava je li tablica s podacima
    provjerit cemo srednji redak - sve stupce
                   zadnji redak- sve stupce
                   srednji redak - 1,
                   srednji redak + 1
                   redak na visini 1/3
                   redak na visini 2/3
                   redak na visini 2/3+1 -stupac na duljini 2/3
    """
    numrows = (df.shape)[0]
    numcols = (df.shape)[1]
    #numcols_middle = numcols/2
    numrows_middle = numrows / 2
    if numrows == 1 or numcols == 1:
        return False
    #ako su samo dva stupca, provjeri desni
    for i in range(numcols):
        if is_number(df.iloc[numrows_middle, i]):
            votes = votes + 1
        if is_number(df.iloc[min(numrows_middle + 1, numrows - 1), i]):
            votes = votes + 1
        if is_number(df.iloc[max(numrows_middle - 1, 1), i]):
            votes = votes + 1
        if is_number(df.iloc[numrows-1, i]):
            votes = votes + 1
        if is_number(df.iloc[min((2 * numrows) / 3, 1), i]):
            votes = votes + 1
        if is_number(df.iloc[min(numrows / 3, 1), i]):
            votes = votes + 1
    if ( votes > VOTES_THRESHOLD ):
        logger.info(u"Is a data table with votes:" + unicode(votes) + u'\n')
        return True
    elif (votes == VOTES_THRESHOLD):
        #ako je prvo slovo naslova broj, vrati true, inace vjerojatno nije tablica
            if (is_number(title[0])):
                logger.info(u"Is a data table with votes:" + unicode(votes) + u', numeric title found!\n')
                return True
            else:
                logger.info(u"Not a data table with votes:" + unicode(votes) + u', no numeric title found!\n')
                return False
    else:
        logger.info(u"Not a data table with votes:" + unicode(votes) + u'\n')
        return False
        
def isLeafTable(table):
    """
    provjerava je li tablica list u DOM stablu
    """
    child_tables = table.find_all("table")
    if len(child_tables) == 0:
        return True
    else:
        return False
        
def extractFromOuterTable(soup):
    """
    izvlaci sadrzaj pocetne tablice (ako je cijeli dokument u tablici)
    """
    #nadji prvu tablicu
    table = soup.find('table')
    if table == None:
        return soup
    if ((table.parent.name == 'div' and (table.parent['class'][0] != 'Section1' if 'class' in table.parent.attrs else True) ) or table.parent == 'body' ):
        return table.extract()
    else:
        return soup
        
def getCleanedText(text):
    """
    ocisti tekst od nekih whitespaceova
    """
    return text.strip().replace("\n", " ").replace("\t", "")#.encode('utf-8')
    
def table2DataFrame(table, logger):
   """
   pretvori bs4 tablicu u pandas dataframe, ociscen od praznih redova
   """
   csv = ''
   rows = table.findAll('tr')
   row_spans = [] 
   try:
       for tr in rows:
           cols = tr.findAll(['th','td'])
           #inicijalizacija row_spans
           if len(row_spans) == 0:
               row_spans=array.array('i',(0,)*130)
           numcol = 0
           for cell in cols:
                colspan = int(cell.get('colspan',1))
                rowspan = int(cell.get('rowspan',1))
                #dok ima rowspan od prije
                while (row_spans[numcol] > 0):
                    #preskoci trenutnu celiju
                    csv += ','
                     #povecaj numcols
                    numcol+=1
                #ako nema colspan ni rowspan
                if ((colspan == 1 or not colspan) and (rowspan==1 or not rowspan)):
                    #dodaj tekst columna
                    csv += u'"{text}"'.format(text=getCleanedText(cell.get_text())) + u','
                #ako ima colspan, nema rowspan
                elif(colspan > 1 and (rowspan==1 or not rowspan)):
                    #dodajemo columne sa ','
                    csv += u'"{text}"'.format(text=getCleanedText(cell.get_text())) + ','*(colspan)
                    numcol+=colspan-1
               #ako ima rowspan, nema colspan
                elif(rowspan > 1 and (colspan==1 or not colspan)):
                    #spremi
                    row_spans[numcol] += rowspan
                    csv += u'"{text}"'.format(text=getCleanedText(cell.get_text())) + ','
                #ako ima obje stvari, dodaj columne, za svaki dodani spremi rowspan
                else:
                    csv += u'"{text}"'.format(text=getCleanedText(cell.get_text())) + ','
                    row_spans[numcol] += rowspan
                    for i in xrange(colspan-1):
                         #povecaj broj columna
                         numcol+=1
                         #dodaj ,
                         csv += u','
                         #spremi rowspan
                         row_spans[numcol] += rowspan
                #prijedi na sljedeci column
                numcol+=1
           #smanji ukupan broj rowspanova u svakom columnu za 1
           if row_spans:
               for i in xrange(len(row_spans)-1,-1,-1):
                   row_spans[i] -= 1
                   if row_spans[i] < 1: row_spans[i]=0
    
           csv += u'\n'
   except IndexError:
        logger.exception(u"Index Error while processing table! Numcols:" + unicode(numcol) + u"Cols found:" + unicode(len(cols)) + u" Rowspan array" + unicode(row_spans) + u"\n Row HTML" + unicode(tr)[:160] + u"\n")
        return None
   stringbuff = StringIO.StringIO(csv)
   DataTable = read_table(stringbuff, delimiter=',', na_values=['z','...'], header=None, thousands = ' ', decimal = ',', encoding='utf-8', error_bad_lines=False)
   return DataTable.dropna(axis=[0,1],how='all')
   

def parseTables(currfile, curr_workbook, csvpath, soup, logger):
    """
    parse tablica iz dokumenta (za svaku tablicu stvara novi worksheet u danom workbooku)
    """
    #nadji vanjsku tablicu(ako postoji)
    #soup = extractFromOuterTable(soup);
    #nadji sve tablice
    tables = soup.find_all('table')
    k = 0
    for table in tables:
            #razmatraj samo listove u slucaju ugnjezdenih tablica
            if (not isLeafTable(table)):
                continue
            table,title = extractTableWithTitle(table);
            #logfile.write("Checking table:" + title + '\n')
            if table != None:# and isNumericTableBS(table, logfile)):
                #try:
                    #obrisi slike (bugfix?);
                    image = table.find('img');
                    if image != None:
                        image.decompose()
                    try:
                        df = table2DataFrame(table, logger)
                    except ValueError:
                            logger.exception(u"Error while processing file: %s :Table: %s\nSkipping...\n", currfile , title[:80])
                            continue
                        
                #except TypeError:
                #        logging.warning("Error while processing file: %s :Table: %s\nSkipping...\n",  currfile, title[:80])
               #         try:
               #             df = table2DataFrame(str(table));
               #             logging.info("Loaded table without encoding from file %s :Table: %s\nContinuing...\n", currfile,title[:80])
               #         except ValueError:
               #             logging.exception("Error while processing file: %s :Table: %s\nSkipping...\n", currfile , title[:80])
               #             continue
            else:
                continue
            logger.info(u"Checking table:" + title[:100] + u'...\n')
            if not df is None and not df.empty and isNumericTable(df, title, logger):
                #create a df with the title
                df2 = DataFrame(data=[title])
                df2 = df2.append(df, ignore_index=True)
                k=k+1
                logger.info(u"Saving table:" + title[:100] + u'\n')  
                df2.to_excel(curr_workbook, sheet_name = unicode(k), header = False, index = False, encoding='utf-8');
                #df.to_csv(csvpath+'-'+str(k)+'.csv', header = False, index = False, encoding='utf-8');
    logger.info(u"Total data tables found:" +  unicode(k) + u'/'+ unicode(len(tables)) + u'\n')
    curr_workbook.save()
    logger.info(u"File processed:" + os.path.abspath(csvpath)+ '\n');
    #print ("File processed:" + os.path.abspath(csvpath)+ '\n');
    curr_workbook.close()


def extractTableWithTitle(table):
    """
    izvlaci tablicu sa naslovom kojeg pronadje prije u stablu
    """
    title_el = table.findPrevious('span')
    title = ''
    elements = 0
    while True:
        elements = elements + 1
        if elements > MAX_TITLE_SEARCH_COUNT:
            break
        elif title_el == None:
            break;
        elif title_el.text == u'\xa0':
            title_el = title_el.find_previous('span');
            continue
        else:
            title = title_el.text + title
            if len(title) == 0  or not title[0].isnumeric():
                title_el = title_el.find_previous('span')
                continue;
            break
        title_el = title_el.find_previous('span')
    return table,getCleanedText(title)

def convertHTMLfile(currfile, destdir, logger):
    """    
    citanje html fajla
    prvi argument je fajl, drugi path di se sprema rezultat
    """
    
    locale.setlocale(locale.LC_ALL,"Croatian") 
    
    if os.path.isfile(currfile):
        try:
            f = open(currfile, 'r')
            #pretvorba dokumenta u BS objekt
            global soup
            soup = BeautifulSoup(f, from_encoding='utf-8')
        except Exception:
            print ("Niste unijeli ispravnu putanju do HTML datoteke!")
            logger.exception(u"Error processing file: %s :\n" , os.path.abspath(currfile))
            return
    else:
        print ("Niste unijeli ispravnu putanju do HTML datoteke!")
        logger.exception(u"Error processing file: %s :\n" , os.path.abspath(currfile))
        return
    if destdir:
        try: 
            os.makedirs(destdir)
        except OSError:
            if not os.path.isdir(destdir):
                print ("Niste unijeli putanju do odredisnog direktorija")
                logger.exception(u"No dir provided!: %s :\n" , os.path.abspath(currfile))
                raise OSError
    else:
        logger.exception(u"Error processing file: %s :\n" , os.path.abspath(currfile)())
        raise OSError
    logger.info(u"Processsing:" + unicode(currfile) + u'\n')
    filepath = getOutputPathFromInputPath(currfile, destdir)
    curr_workbook = ExcelWriter(filepath + '.xlsx');
    #parse tablica (za svaku podatkovnu tablicu stvara novi worksheet)
    parseTables(os.path.abspath(currfile), curr_workbook, filepath, soup, logger)