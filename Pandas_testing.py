# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 19:16:08 2015

@author: Antonio
"""

from bs4 import BeautifulSoup
from pandas import read_html
from pandas import ExcelWriter

currfile = 'E:/praksa-101/DATA/HTML_prema_programu/2005/4/4/3/4-4-3_1h2005$Nekomercijalni-turisticki-promet-u-kucama-i-stanovima-za-odmor-u-2005.htm';

f = open(currfile, 'r')

soup = BeautifulSoup(f, from_encoding='utf-8')
tables = soup.find_all('table')
for table in tables:
    DataTable = read_html(unicode(table), thousands = None, encoding='utf-8', infer_types=True);