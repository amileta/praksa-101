# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 12:34:38 2015

@author: Antonio
"""

import ScrapeTablesFileSystem
import logging
import sys


class StreamToLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
 
   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())

#logfile for debugging
#logging.basicConfig(level=logging.DEBUG, filename='./tmp/exception.log')

filepaths = [
              "E:/praksa-101/DATA/HTML_prema_programu/2001/12/1/1/12-1-1_h2001$Procjena-tromjesecnog-obracuna-bruto-domaceg-proizvoda-u-2001.htm"
              #"E:\\praksa-101\\DATA\\HTML_prema_programu\\2002\\2\\1\\2\\2-1-2_3h2002$Zaposleni-radnici-i-proizvodnost-rada-u-industriji-u-2002.htm"
              #"E:\\praksa-101\\TEST\\2-1-3_6h2006$Indeksi-obujma-industrijske-proizvodnje-u-2006-desezonirane-vremenske-serije-i-trend-indeksi.htm"
              #"E:\\praksa-101\\DATA\\HTML_prema_programu\\2002\\2\\1\\2\\2-1-2_5h2002$Zaposleni-radnici-i-proizvodnost-rada-u-industriji-u-2002.htm"
              #"E:\\praksa-101\\DATA\\HTML_prema_programu\\2013\\1\\1\\21\\01-01-21_01_2013$Bazno-istrazivanje-o-strukturi-vocnjaka-konacni-podaci-Stanje-1-lipnja-2012.htm"
#             "E:\\praksa-101\\DATA\\HTML_prema_programu\\2007\\4\\4\\1\\4-4-1_1h2007$Turizam-u-2007.htm"
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_1h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_5h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_6h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_7h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_9_9h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/1/4-2-1_10h2001$Robna-razmjena-RH-s-inozemstvom-u-2001.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2001/4/2/4/4-2-4_2h2001$Robna-razmjena-RH-s-inozemstvom-u-2001-konacni-podaci.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2009/6/1/1/6-1-1_1h2009$Koristenje-voda-i-zastita-voda-od-zagaivanja-u-industriji-u-2008.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2004/12/2/1/12-2-1_1h2004$Investicije-u-2003.htm",
#             "E:/praksa-101/DATA/HTML_prema_programu/2004/10/1/1/10-1-1h2004$Punoljetni-pocinitelji-kaznenih-djela-prema-vrsti-odluke-u-2003.htm"
            ]
destdir = './TEST'
### LOGGING CONFIGURATION ###
# create logger
logger = logging.getLogger('scraping_logger')
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.FileHandler('./reparsed_s_greskama.log')
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

sl = StreamToLogger(logger, logging.INFO)
sys.stdout = sl
 
sl = StreamToLogger(logger, logging.ERROR)
sys.stderr = sl

try:
    for path in filepaths:
          ScrapeTablesFileSystem.convertHTMLfile(path, destdir, logger)
except Exception as ex:
    print ex.message
    logging.exception("Error processing file: " + path + ":\n" +ex.message + '\n')