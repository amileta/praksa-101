# -*- coding: utf-8 -*-
""" Testiranje parsera """

from bs4  import BeautifulSoup
from pandas import read_table
from pandas import ExcelWriter
import cStringIO
import array

def table2csv(html_txt):
   csvs = []

   for table in tables:
       csv = ''
       rows = table.findAll('tr')
       row_spans = []
       do_ident = False

       for tr in rows:
           cols = tr.findAll(['th','td'])

           for cell in cols:
               colspan = int(cell.get('colspan',1))
               rowspan = int(cell.get('rowspan',1))

               if do_ident:
                   do_ident = False
                   csv += ','*(len(row_spans))

               if rowspan > 1: row_spans.append(rowspan)

               csv += u'"{text}"'.format(text=unicode(cell.text)) + u','*(colspan)

           if row_spans:
               for i in xrange(len(row_spans)-1,-1,-1):
                   row_spans[i] -= 1
                   if row_spans[i] < 1: row_spans.pop()

           do_ident = True if row_spans else False

           csv += '\n'
       csvs.append(csv)
       #print csv
   return '\n\n'.join(csvs)
   
def extractFromOuterTable(soup):
    #nadji prvu tablicu
    table = soup.find('table')
    if table == None:
        return soup
    if ((table.parent.name == 'div' and (table.parent['class'][0] != 'Section1' if 'class' in table.parent.attrs else True) ) or table.parent == 'body' ):
        return table.extract()
    else:
        return soup
        
def table2DataFrame(table):
   csv = ''
   rows = table.findAll('tr')
   row_spans = [] 
   for tr in rows:
       cols = tr.findAll(['th','td'])
       #inicijalizacija row_spans
       if len(row_spans) == 0:
           row_spans=array.array('i',(0,)*120)
       numcol = 0
       for cell in cols:
            colspan = int(cell.get('colspan',1))
            rowspan = int(cell.get('rowspan',1))
            #dok ima rowspan od prije
            while (row_spans[numcol] > 0):
                #preskoci trenutnu celiju
                csv += ','
                 #povecaj numcols
                numcol+=1
            #ako nema colspan ni rowspan
            if ((colspan == 1 or not colspan) and (rowspan==1 or not rowspan)):
                #dodaj tekst columna
                csv += '"{text}"'.format(text=cell.get_text().strip().replace("\n", " ").replace("\t", "").encode('utf-8')) + ','
            #ako ima colspan, nema rowspan
            elif(colspan > 1 and (rowspan==1 or not rowspan)):
                #dodajemo columne sa ','
                csv += '"{text}"'.format(text=cell.get_text().strip().replace("\n", " ").replace("\t", "").encode('utf-8')) + ','*(colspan)
                numcol+=colspan-1
           #ako ima rowspan, nema colspan
            elif(rowspan > 1 and (colspan==1 or not colspan)):
                #spremi
                row_spans[numcol] += rowspan
                csv += '"{text}"'.format(text=cell.get_text().strip().replace("\n", " ").replace("\t", "").encode('utf-8')) + ','
            #ako ima obje stvari, dodaj columne, za svaki dodani spremi rowspan
            else:
                csv += '"{text}"'.format(text=cell.get_text().strip().replace("\n", " ").replace("\t", "").encode('utf-8')) + ','
                row_spans[numcol] += rowspan
                for i in xrange(colspan-1):
                     #povecaj broj columna
                     numcol+=1
                     #dodaj ,
                     csv += ','
                     #spremi rowspan
                     row_spans[numcol] += rowspan
            #prijedi na sljedeci column
            numcol+=1
       #smanji ukupan broj rowspanova u svakom columnu za 1
       if row_spans:
           for i in xrange(len(row_spans)-1,-1,-1):
               row_spans[i] -= 1
               if row_spans[i] < 1: row_spans[i]=0

       csv += '\n'
   stringbuff = cStringIO.StringIO(csv)
   DataTable = read_table(stringbuff, delimiter=',', na_values=['z','...'], header=None, thousands = ' ', decimal = ',', encoding='utf-8', error_bad_lines=False)
   return DataTable.dropna(axis=[0,1],how='all')
           
original=open("E:\\praksa-101\\DATA\\HTML_prema_programu\\2013\\1\\1\\21\\01-01-21_01_2013$Bazno-istrazivanje-o-strukturi-vocnjaka-konacni-podaci-Stanje-1-lipnja-2012.htm", 'r')
soup = BeautifulSoup(original)
soup = extractFromOuterTable(soup)
tables = soup.findAll('table')
curr_workbook = ExcelWriter('results.xlsx');
try:
    for table in tables:
        csvs = table2DataFrame(table)
        csvs.to_excel(curr_workbook, sheet_name = 'Test', header = False, index = False, encoding='utf-8');
except IOError as ex:
    print ex.message
#df.to_csv(csvpath+'-'+str(k)+'.csv', header = False, index = False, encoding='utf-8');
curr_workbook.save()
curr_workbook.close()