# -*- coding: utf-8 -*-
"""
Created on Sun May 17 18:02:32 2015

@author: Administrator
"""

import sys
import getopt
import os
import logging
import re
import pandas.io.excel as pd
from pandas import ExcelWriter
from openpyxl import load_workbook

### FUNKCIJE ###

def cleanText(cell):
    """
    Ako je ustanovljeno da je celija samo tekstualna, samo se uklanja visak razmaka
    i oznake indeksa
    """
    cell = ' '.join(unicode(cell).split())
    #za eliminaciju Ä‡elija gdje su podaci oznaÄŤeni s '-', ne treba jer Excelu ne smeta!
    #if cell == '-':
    #    return ''
    cell = re.sub("\d\)+",'',cell)
    return cell