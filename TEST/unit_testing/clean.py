# -*- coding: utf-8 -*-
"""
Created on Sun May 17 23:57:32 2015

@author: Administrator
"""

import sys
import getopt
import os
import logging
import re
import pandas.io.excel as pd
from pandas import ExcelWriter
from openpyxl import load_workbook

### FUNKCIJE ###

def to_number(s):
    try:
        s1 = float(s)
        return s1, True
    except ValueError:
        return s, False

def cleanText(cell):
    """
    Ako je ustanovljeno da je celija samo tekstualna, samo se uklanja visak razmaka
    i oznake indeksa
    """
    cell = ' '.join(unicode(cell).split())
    #za eliminaciju Ä‡elija gdje su podaci oznaÄŤeni s '-', ne treba jer Excelu ne smeta!
    #if cell == '-':
    #    return ''
    cell = re.sub("\d\)+",'',cell)
    return cell


def clean(cell):
    """
    Ciscenje celije, ako nije nekakva cudna stvar tipa godina ili vise njih
    pokusava konverziju u float, ako ne uspije zadrzava kao tekst
    """
    if cell is None:
        return None
    cell = unicode(cell)
    
    #1 provjeri da nije nesto tipa "2006.   2007."
    temp = ' '.join(cell.split())
    years = re.match("^\d{4}.?\s\d{4}.?$",temp)
    if years is not None:
        return cleanText(cell)
    #makni razmake
    temp = cell.strip()
    #1 provjeri da nije nesto tipa "2006."
    years = re.match("^\d{4}.$",temp)
    if years is not None:
        return temp
    #probaj konvertirat u float (samo bez razmaka)
    temp = ''.join(cell.split())
    result, success = to_number(temp)
    if success:
        return result
    #makni i decimalne tocke
    temp = temp.replace('.', '')
    #probaj konvertirat u float (bez razmaka i decimalnih toÄŤki)
    result, success = to_number(temp)
    if success:
        return result
    #zamijeni decimalne zareze
    temp = temp.replace(',', '.')
    #probaj konvertirat u float (bez razmaka i sa decimalnim toÄŤkama)
    result, success = to_number(temp)
    if success:
        return result
    else:
        return cleanText(cell)