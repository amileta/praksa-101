# -*- coding: utf-8 -*-
"""
Created on Sun May 17 23:57:32 2015

@author: Administrator
"""

import sys
import getopt
import os
import logging
import re
import pandas.io.excel as pd
from pandas import ExcelWriter
from openpyxl import load_workbook

### FUNKCIJE ###

def cleanFile2(filename, dirpath, logger):
    """
    Ciscenje datoteke pomocu openpyxl knjiznice za rad s Excel datotekama
    """
    filepath = os.path.join(dirpath,filename)   
    wb2 = load_workbook(filepath)
    for sheet in wb2.sheetnames:
        worksheet = wb2.get_sheet_by_name(sheet)
        i = 1
        for row in worksheet.rows:
            for col in range(1, len(row)+1):
                cell = worksheet.cell(row = i, column = col)
                result = clean (cell.value)
                if result is not None:
                    cell.value = result
            i+=1
    wb2.save(filepath)
    logger.info(u"File cleaned:" + os.path.abspath(filepath)+ u'\n'); 