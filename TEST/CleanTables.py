# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 22:38:56 2015

Skripta za ciscenje Excel datoteka koje smo prethodno konvertirali iz HTML-a.

Upute za koristenje:

Kopirati u top level direktorij u kojem se nalazi direktorij s datotekama koje 
treba ocistiti i pokrenuti (default postavke)

ILI

Pokrenuti preko komandne linije, sa opcionalnim argumentima za konfiguraciju:

CleanTables.py -d <startdir> -e <extension> -l <logfile> -i <ignorestring>

-d    pocetni direktorij
-e    ekstenzija Excel datoteke (WIP, zasad samo .xlsx radi)
-l    putanja do log datoteke
-i    string koji odreduje koje direktorije ignorirati (imena moraju biti tocna)
      i odvojena zarezima

@author: Antonio
"""

import sys
import getopt
import os
import logging
import re
import pandas.io.excel as pd
from pandas import ExcelWriter
from openpyxl import load_workbook

### FUNKCIJE ###

def to_number(s):
    try:
        s1 = float(s)
        return s1, True
    except ValueError:
        return s, False
        
def cleanText(cell):
    """
    Ako je ustanovljeno da je celija samo tekstualna, samo se uklanja visak razmaka
    i oznake indeksa
    """
    cell = ' '.join(unicode(cell).split())
    #za eliminaciju ćelija gdje su podaci označeni s '-', ne treba jer Excelu ne smeta!
    #if cell == '-':
    #    return ''
    #za eliminaciju oznaka indeksa
    #cell = re.sub("\d\)+",'',cell)
    return cell

def clean(cell):
    """
    Ciscenje celije, ako nije nekakva cudna stvar tipa godina ili vise njih
    pokusava konverziju u float, ako ne uspije zadrzava kao tekst
    """
    if cell is None:
        return None
    cell = unicode(cell)
    
    #1 provjeri da nije nesto tipa "2006.   2007."
    temp = ' '.join(cell.split())
    years = re.match("^\d{4}\.?\s\d{4}\.?$",temp)
    if years is not None:
        return cleanText(cell)
    #makni razmake
    temp = cell.strip()
    #1 provjeri da nije nesto tipa "2006."
    years = re.match("^\d{4}\.$",temp)
    if years is not None:
        return temp
    #probaj konvertirat u float (samo bez razmaka)
    temp = ''.join(cell.split())
    result, success = to_number(temp)
    if success:
        return result
    #makni i decimalne tocke
    temp = temp.replace('.', '')
    #probaj konvertirat u float (bez razmaka i decimalnih točki)
    result, success = to_number(temp)
    if success:
        return result
    #zamijeni decimalne zareze
    temp = temp.replace(',', '.')
    #probaj konvertirat u float (bez razmaka i sa decimalnim točkama)
    result, success = to_number(temp)
    if success:
        return result
    else:
        return cleanText(cell)
    
def cleanFile(filename, dirpath, logger):
    """
    Ciscenje datoteke loadanjem u Pandas - DEPRECATED!
    (ne radi u nekim slucajevima, vidi https://github.com/pydata/pandas/issues/5891)
    """
    
    filepath = os.path.join(dirpath,filename)   
    sheet_dfs = pd.read_excel(filepath, header = None, sheetname=None, convert_float=False) 
    curr_workbook = ExcelWriter(filepath);    
    k=0
    for key,df in sheet_dfs.items():
        try:
            df = df.applymap(clean)
        except Exception as ex:
            logger.error(ex)
        #write it back
        k+=1
        df.to_excel('test.xlsx', sheet_name=str(k), header=False, index=False)
        curr_workbook.save()
    logger.info(u"File cleaned:" + str(os.path.abspath(filepath))+ u'\n');
    #print ("File processed:" + os.path.abspath(csvpath)+ '\n');
    curr_workbook.close()
    return

def cleanFile2(filename, dirpath, logger):
    """
    Ciscenje datoteke pomocu openpyxl knjiznice za rad s Excel datotekama
    """
    filepath = os.path.join(dirpath,filename)   
    wb2 = load_workbook(filepath)
    for sheet in wb2.sheetnames:
        worksheet = wb2.get_sheet_by_name(sheet)
        i = 1
        for row in worksheet.rows:
            for col in range(1, len(row)+1):
                cell = worksheet.cell(row = i, column = col)
                result = clean (cell.value)
                if result is not None:
                    cell.value = result
            i+=1
    wb2.save(filepath)
    logger.info(u"File cleaned:" + str(os.path.abspath(filepath))+ u'\n');      


def main(argv):
    # The top argument for walk
    TOPDIR = '.'
    # The extension to search for
    EXTEN = '.xlsx'
    # Directories to ignore
    IGNORE = ['Podaci_prema_programu','IGNORE', '2001','2002', '2003', '2004', '2005','2006','2008','2009','2010','2011','2012', '2013', '2014', '2015']
    #logfile for processed files
    CLEANEDLOG = 'cleaned.log'    
    
    ### COMMAND LINE OPCIJE ###
    
    try:
        opts, args = getopt.getopt(argv,"hd:e:l:i:")
    except getopt.GetoptError:
        print 'CleanTables.py -d <startdir> -e <extension> -l <logfile> -i <ignorestring>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'CleanTables.py -d <startdir> -e <extension>'
            sys.exit()
        elif opt in ("-d"):
            TOPDIR = arg
        elif opt in ("-e"):
            EXTEN = arg
        elif opt in ("-l"):
            CLEANEDLOG = arg
        elif opt in ("-i"):
            IGNORE = arg.split(',')

    ### LOGGING CONFIGURATION ###
    
    # create logger
    logger = logging.getLogger('cleaning_logger')
    logger.setLevel(logging.DEBUG)
    # create console handler and set level to debug
    ch = logging.FileHandler(CLEANEDLOG)
    ch.setLevel(logging.DEBUG)
    
    # create formatter
    formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')
    
    # add formatter to ch
    ch.setFormatter(formatter)
    
    # add ch to logger
    logger.addHandler(ch)


    ### RECURSIVE PROCESS ###
    for dirpath, dirnames, files in os.walk(TOPDIR):
        print "Prociscavam direktorij: " + str(dirpath)
        # Remove directories in ignore
        # directory names must match exactly!
        for idir in IGNORE:
            if idir in dirnames:
                dirnames.remove(idir)
        #loop through files
        print "Pronasao " + str(len(files)) + " datoteka."
        for name in files:
            if name.lower().endswith(EXTEN):
                #call the clean function for that file
                cleanFile2(name,dirpath,logger)
                print "Ociscena datoteka: " + str(name) 
        #processedpaths.write(str(results))
        #results=''
    sys.stdout = sys.__stdout__
    sys.stderr = sys.__stderr__
    ### OSLOBADANJE RESURSA ###
    ch.close()

### PROGRAN START ###

if __name__ == "__main__":
   main(sys.argv[1:])