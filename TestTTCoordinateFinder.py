# -*- coding: utf-8 -*-
"""
Created on Mon May 11 10:28:11 2015

@author: Antonio
"""
import logging
import TableTitleCoordinateFinder

### LOGGING CONFIGURATION ###

# create logger
logger = logging.getLogger('pdf_test_finder_logger')
logger.setLevel(logging.DEBUG)
# create console handler and set level to debug
ch = logging.FileHandler("test-finder.log")
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(levelname)s - %(asctime)s - %(message)s')

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

tables = TableTitleCoordinateFinder.find("E:\praksa-101\TEST\SI-1468_13.pdf",".",logger)
print tables